#!/usr/bin/env bash

# Script to install wide system nodejs-lts on Debian

# Author: Enrique MP 

sudo curl -fsSL https://deb.nodesource.com/setup_18.x | sudo bash;
sudo apt-get install -y nodejs
