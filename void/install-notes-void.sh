#!/usr/bin/env sh

#
# Script: My first shell script
# Author: Enrique MP
#

# sale del script si encuentra un error sin ejecutar el resto de instrucciones
set -o errexit

# Primero actualizamos el gestor de paquetes y el sistema
sudo xbps-install -Su
sudo xbps-install -u xbps

# Instalamos paquetes base para Kde Plasma
sudo xbps-install \
                kde5\ # paquete base
                xorg \ # sistema gráfico
                kde5-baseapps \ # kate, konsole y dolphin
                octoxbps \ # opcional, GUI para xbps
                base-devel \
                pulseaudio \
                sddm \
                cups


sudo ln -s /etc/sv/dbus/ /var/service
sudo ln -s /etc/sv/bluetoothd/ /var/service
sudo ln -s /etc/sv/NetworkManager/ /var/service
sudo usermod -aG bluetooth "$USER"
sudo ln -s /etc/sv/cupsd/ /var/service
sudo ln -s /etc/sv/sddm/ /var/service

# Reboot

## Ver servicios activados
# sudo sv status /var/service/*
# sudo rm /var/service/agetty-tty4
# sudo rm /var/service/agetty-tty5
# sudo rm /var/service/agetty-tty6
# sudo xbps-install neofetch
# sudo vkpurge list
# sudo vkpurge rm 5.19.10_1
# sudo xbps-remove -Oov
