# Debian install

This is a note to store TODO's and collects snippets to develop my Debian Installation.


## UI tips

### To hide the mouse pointer while typing

>Thanks a lot! On Linux, a similar utility is available called unclutter that hides the pointer system-wide.

```sh
sudo apt-get install unclutter
unclutter -idle 1 -root # for hiding after a one-second delay
```
