#!/usr/bin/env bash

# Installation script for get Alacritty in Debian
# Author: Enrique MP

# Creating a temp directory for building
tmp_dir=$( mktemp -d -t alacritty-builder-XXX )

# Function to clean the tmp directory created before
clean_up() {
    test -d "$tmp_dir" && rm -rf "$tmp_dir"
}

# Dependencies for Debian systems
sudo apt install -y \
    git \
    cmake \
    pkg-config \
    libfreetype6-dev \
    libfontconfig1-dev \
    libxcb-xfixes0-dev \
    libxkbcommon-dev \
    python3

# Install 'rustup' as a dependancy of cargo
sudo apt install -y curl &&
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh &&
bash -c 'source $HOME/.cargo/env' &&
rustup override set stable && rustup update stable

# Downloading alacritty sources
git clone https://github.com/alacritty/alacritty.git "$tmp_dir"
cd "$tmp_dir" || exit

# It's time to building
cargo build --release || echo "Something went wrong!"

# Post-build, yet inside the building directory
infocmp alacritty;
sudo tic -xe "alacritty, alacritty-direct extra/alacritty.info";

# Desktop entry
sudo cp "$tmp_dir"/target/release/alacritty /usr/local/bin; # or anywhere else in $PATH
sudo cp "$tmp_dir"/extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg;
sudo desktop-file-install "$tmp_dir"/extra/linux/Alacritty.desktop;
sudo update-desktop-database;

# Man page
sudo mkdir -p /usr/local/share/man/man1;
gzip -c "$tmp_dir"/extra/alacritty.man | \
sudo tee /usr/local/share/man/man1/alacritty.1.gz > /dev/null;
gzip -c "$tmp_dir"/extra/alacritty-msg.man | \
sudo tee /usr/local/share/man/man1/alacritty-msg.1.gz > /dev/null;

# Bash completions
mkdir -p ~/.bash_completion;
cp "$tmp_dir"/extra/completions/alacritty.bash ~/.bash_completion/alacritty;
echo "source ~/.bash_completion/alacritty" >> ~/.bashrc;

# make directory for config
mkdir -p ~/.config/alacritty
mkdir -p ~/.config/alacritty/themes
git clone https://github.com/alacritty/alacritty-theme ~/.config/alacritty/themes

printf 'Remember to see how to install Alacritty completions for your shell \n'

# Cleaning temp directory
# trap 'clean_up $tmp_dir' exit
clean_up "$tmp_dir" && exit

